package com.example.PPMTool.services;

import com.example.PPMTool.domain.Project;
import com.example.PPMTool.exceptions.ProjectIdException;
import com.example.PPMTool.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    //Lot of logic is yet to come here

    public Project saveOrUpdateProject(Project project){

        try{
            return projectRepository.save(project);
        }
        catch (Exception e){
            throw new ProjectIdException("Project ID '"+project.getProjectIdentifier().toUpperCase()+"' already exist");
        }

    }
}
